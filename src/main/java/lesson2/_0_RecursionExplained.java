package lesson2;

public class _0_RecursionExplained {
   static int depth = 0;
   static boolean showRecursion = true;


   static int fact(int number) {
      return factTailRecursionOptimizedToLoop(number, 1);
   }

   private static int factTailRecursionOptimizedToLoop(int number, int multiplier) {
      int m = multiplier;
      for (int n = number; n >= 0; n--) {
         if (n == 0) return m;
         int prevM = m;
         m = n * prevM;
         System.out.println(m + " = " + n + " * " + prevM);
      }

      return m;
   }

   public static void main(String[] args) {
      System.out.println(fact(10));
   }
}
