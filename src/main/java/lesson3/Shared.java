package lesson3;

import java.io.File;
import java.util.Scanner;
import java.util.UUID;

public class Shared {

   public static final String filePath = "/Users/vach/IdeaProjects/acajava/target/work/some-file.smth";

   public static void readLine() {
      new Scanner(System.in).nextLine();
   }

   public static String randomString() {
      return UUID.randomUUID().toString();
   }

   public static void deleteFile(String path) {
      new File(path).delete();
   }

   public static void log(String message) {
      System.out.println(System.currentTimeMillis() + " | " + Thread.currentThread().getName() + " | " + message);
   }

}

