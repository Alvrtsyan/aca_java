package lesson1.overrideVsOverload;


class A {
   void m1() {
   }
}

final class B extends A {
   @Override
   void m1() {
      // override
   }

   void m1(int someArg){
      // overload (since method has a different signature)
   }
}