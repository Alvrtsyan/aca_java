package lesson1._chess.interfaces;

public interface IChessBoard {

   boolean canMove(IPosition position1, IPosition position2);

   void move(IPosition position1, IPosition position2);

   IChessFigure getFigureAt(IPosition position);

}
