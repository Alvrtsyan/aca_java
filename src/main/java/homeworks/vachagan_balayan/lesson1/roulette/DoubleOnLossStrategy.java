package homeworks.vachagan_balayan.lesson1.roulette;

import lesson1._roulette.interfaces.IRoulette.Color;
import lesson1._roulette.interfaces.IStrategy;

import static lesson1._roulette.interfaces.IRoulette.Color.BLACK;
import static lesson1._roulette.interfaces.IRoulette.Color.RED;

public class DoubleOnLossStrategy implements IStrategy {

   private class Bet implements IBet {

      private final int amount;
      private final Color color;

      public Bet(int amount, Color color) {
         this.amount = amount;
         this.color = color;
      }

      @Override
      public int getAmount() {
         return amount;
      }

      @Override
      public Color getColor() {
         return color;
      }
   }

   private Color lastColor = BLACK;
   private int lastAmount = 1;
   private boolean didIWinLastTime = false;

   @Override
   public void registerSuccess() {
      this.didIWinLastTime = true;
   }

   @Override
   public void registerFailure() {
      this.didIWinLastTime = false;
   }

   @Override
   public IBet nextBet() {
      Color color;
      int amount;

      if (didIWinLastTime) {
         amount = 1;
         color = lastColor;
      } else {
         amount = lastAmount * 2;
         color = inverseOf(lastColor);
      }

      lastColor = color;
      lastAmount = amount;

      return new Bet(amount, color);
   }

   private static Color inverseOf(Color color) {
      return (color == RED) ? BLACK : RED;
   }
}
